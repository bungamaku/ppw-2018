from django.shortcuts import render
from datetime import datetime, date
# Bunga Amalia Kurniawati - 1706022104
mhs_name = 'Bunga Amalia Kurniawati'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 10, 18)
npm = 1706022104
study = ['computer science', 'Universitas Indonesia']
hobby = 'cooking bento'
description = 'a full-time learner, part-time coder'
img = 'https://media.licdn.com/dms/image/C5603AQGwXRJg7_qdTQ/profile-displayphoto-shrink_200_200/0?e=1541635200&v=beta&t=StNWFDUAqadUz9QqST8sAwvotaB3T1fK48Xcm4rJFU0'
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'major' : study[0],
                'school' : study[1], 'hobby' : hobby, 'desc' : description, 'image' : img}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
